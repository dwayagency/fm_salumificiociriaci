<?php
/**
 * Template Name: Template territorio Salumificio Ciriaci
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<?php get_header(); ?>
<div class="container-fluid blocco-1-territorio">
<div class="row">
<img class="sfondo8" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/territorio-blocco-1.png" alt="Immagine sfondo territorio Salumificio Ciriaci blocco 1" />
<div class="testo-slider-territorio">  
  <h4>Nel cuore<br/><sup>della</sup> <span class="territorio-valdaso"><sub>Valdaso.</sub></h4>
</div>
 <img class="sfondo9" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/blocco-1-territorio.png" alt="Sfondo blocco 1" />   

</div>
</div>
<div class="container-fluid blocco-2-territorio">
    <h4>NEL CUORE DELLA VALDASO</h4>
    <h5>Nel cuore dei consumatori</h5>
    <p class="foglia-verde"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/foglia-verde-blocco-2-territorio.png" alt="Foglia verde blocco 2 territorio" /></p>
    <div class="container"><p class="testo-1-blocco-2-territorio">Immersi in un paesaggio unico <strong>nel cuore della Valdaso,</strong> in provincia di Fermo, si staglia in una struttura moderna lo
stabilimento produttivo Ciriaci. È in questa terra operosa che la famiglia Ciriaci negli <strong>anni ‘70</strong> avviò l’attività di
produzione artigianale di affettati e insaccati.</p>
    <p class="testo-2-blocco-2-territorio">Nel tempo questo nome è diventato vero e proprio simbolo della <strong>qualità</strong> che la contraddistingue.
La sede di Ortezzano è all’avanguardia e di grandi dimensioni con un ampio punto vendita, dove si intuisce la cura e la
<strong>passione di un’azienda capace di interpretare i cambiamenti senza perdere i valori e le tradizioni.</strong></p>
</div>
<div class="row">
<div class="col-8">
  <img class="immagine-sfondo-maiali-blocco-2-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/immagine-sfondo-maiali-blocco-2-territorio.png" /></div>
<div class="col-4"></div>
</div>
<div class="row">
  <div class="col-1"></div>
  <div class="col-8"><img class="immagine-maiali-blocco-2-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/maiali-blocco-2-territorio.png" alt="I maiali del Salumificio Ciriaci" /></div>
  <div class="col-3"></div>
</div>
<div class="row">
  <div class="col-5"></div>
  <div class="col-5 descrizione-maiali-blocco-2-territorio"><p>
  I maiali sono nati e allevati presso l’Azienda Agricola costituita da due sedi: quella di <strong>Ripatransone</strong>, dove i suinetti 
nascono e rimangono ﬁno allo svezzamento, e l’altra sita a <strong>Monteﬁore dell’Aso</strong> in cui si svolge la fase di ingrasso. Gli 
animali sono controllati quindi dalla nascita ﬁno alla maturità, seguendo una dieta particolare per l’ottenimento di una carne di alta qualità, e crescendo in condizioni idonee nel rispetto della normativa vigente.
<br/><br/>
I suini pesanti, cioè quelli che hanno raggiunto i 160-180 kg di peso, vengono destinati alla macellazione presso il mattatoio del Salumiﬁcio Ciriaci srl, nella sede di Ortezzano (FM), in possesso del bollo di riconoscimento CEE 1900M.    
</p>
  </div>
  <div class="col-2"></div>
</div>

</div>
<div class="container-fluid blocco-3-territorio">
<h4>D.O.P. e I.G.P.</h4>
    <h5>Marchi e tutele</h5>
    <div class="container">
      <div class="row">
        <div class="col-4"></div>
        <div class="col-8"><img class="immagine-sfondo-maiali-blocco-3-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/immagine-sfondo-maiali-blocco-2-territorio.png" /></div>
      </div>
      <div class="row">
        <div class="col-3"></div>
        <div class="col-8"><img class="immagine-maiali-blocco-3-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/salumi-blocco-3-territorio.png" alt="I salumi del Salumificio Ciriaci" /></div>
        <div class="col-1"></div>
</div>
<div class="row">
  <div class="col-7 blocco-dop"><h4 class="dop">D.O.P.</h4>
<p class="descrizione-dop">
Il marchio DOP (<strong>Denominazione di Origine Protetta</strong>) è una
denominazione registrata presso la Comunità Europea per
indicare che un prodotto è di <strong>alta qualità</strong>. La Marche e le
tradizioni utilizzate per i nostri salumi li rendono così peculiari
da doverli salvaguardare da contraffazioni. Il suo regolamento
molto limitato garantisce al consumatore un prodotto dalle
eccellenti proprietà, sia per quanto riguarda l’origine e la
provenienza delle materie prime, sia per la sua produzione e
la sicurezza alimentare.<br/><br/>
La denominazione DOP nasce assieme a quella IGP nel 1992
grazie al Regolamento CEE 2081/92 emanato dalla Comunità
Europea.
</p>
</div>
  <div class="col-5"></div>
</div>
</div>
<div class="container riga-2-blocco-4-territorio">
  <div class="row">
  <div class="col-8"><img class="immagine-sfondo-maiali-blocco-3-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/immagine-sfondo-maiali-blocco-2-territorio.png" /></div>
  <div class="col-4"></div>
</div>
<div class="row">
  <div class="col-1"></div>
  <div class="col-8"><img class="immagine-maiali-blocco-3-territorio" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/territorio/salumi-blocco-4-territorio.png" alt="I salumi del Salumificio Ciriaci" /></div>
  <div class="col-3"></div>
</div>
<div class="row">
<div class="col-5"></div> 
<div class="col-7 blocco-igp"><h4 class="igp">I.G.P.</h4>
<p class="descrizione-igp">
l marchio IGP (<strong>Indicazione Geografica Protetta</strong>) di tutela
giuridica viene attribuito dall’Unione europea agli alimenti le
cui peculiari caratteristiche qualitative dipendono
essenzialmente o esclusivamente dal territorio in cui sono stati
prodotti. Chi produce IGP deve attenersi alle rigide regole
produttive stabilite nel disciplinare di produzione e il rispetto
di tali regole è garantito da uno specifico organismo di
controllo.
</p>
</div>
</div>
</div>
</div>
<?php get_footer(); ?>