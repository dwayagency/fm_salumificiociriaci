<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<script>
	function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }
</script>
<link rel="stylesheet" href="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/assets/owl-carousel/assets/owl.carousel.css" type="text/css">
<link rel="stylesheet" href="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/assets/owl-carousel/assets/owl.theme.default.min.css" type="text/css">
<link rel="stylesheet" href="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/assets/owl-carousel/assets/owl.theme.green.min.css" type="text/css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-4' ); ?></a>

	
<header id="masthead" class="site-header <?php if ( get_theme_mod( 'sticky_header', 0 ) ) : echo 'sticky-top'; endif; ?>">
<div class="row">
<div class="col-3 colonna-menu">
	<nav id="site-navigation" class="main-navigation navbar navbar-expand-lg navbar-dark bg-dark">
			<?php if( get_theme_mod( 'header_within_container', 0 ) ) : ?><div class="container"><?php endif; ?>
				

				

				<button onclick="toggle_visibility('menu_principale');" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#primary-menu-wrap" aria-controls="primary-menu-wrap" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<?php
					wp_nav_menu( array(
						'theme_location'  => 'menu-1',
						'menu_id'         => 'primary-menu',
						'container'       => 'div',
						'container_class' => 'collapse navbar-collapse navbar-ex1-collapse ',
						'container_id'	  => 'menu_principale',
						'menu_class'      => 'nav navbar-nav  ',
			            'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
			            'depth'           => 5,
			            'walker'          => new wp_bootstrap_navwalker())
					 );
				?>
			<?php if( get_theme_mod( 'header_within_container', 0 ) ) : ?></div><!-- /.container --><?php endif; ?>
		</nav><!-- #site-navigation -->
					</div>
					
					</div>	
	</header><!-- #masthead -->

	<div id="content" class="site-content">
