<?php
/**
 * Template Name: Template filiera Salumificio Ciriaci
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<?php get_header(); ?>
<div class="container-fluid blocco-1-filiera">
<div class="row">
<img class="sfondo10" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/filiera/filiera-blocco-1.png" alt="Immagine sfondo filiera Salumificio Ciriaci blocco 1" />
<div class="testo-slider-filiera">  
  <h4>I nostri<br/><span class="filiera-allevamenti">allevamenti.</h4>
</div>
 <img class="sfondo11" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/filiera/blocco-1-filiera.png" alt="Sfondo blocco 1" />   

</div>
</div>
<?php get_footer(); ?>