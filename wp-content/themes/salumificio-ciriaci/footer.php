<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer text-center bg-white mt-4 text-muted">

	<div class="container-fluid blocco-8-homepage">
	
<a href="//salumificiociriaci.it"><img class="logo-footer" src="//salumificiociriaci.it/wp-content/uploads/2019/07/Logo_A17_Rectangle_2_pattern.png" alt="logo salumificio ciriaci" /></a>
<div class="col-12 social-footer">
<a href="#"><i class="fa fa-facebook-f"></i></a><a href="#"><i class="fa fa-instagram"></i></a><a href="#"><i class="fa fa-twitter"></i></a>
</div>
<div class="col-12 menu-footer">
	<ul id="footer-menu">
		<li><a href="//salumificiociriaci.it/privacy-policy">PRIVACY POLICY</a></li>
		<li><a href="//salumificiociriaci.it/cookie-policy">COOKIE POLICY</a></li>
		<li><a href="//salumificiociriaci.it/lavora-con-noi">LAVORA CON NOI</a></li>
		<li><a href="//salumificiociriaci.it/newsletter">NEWSLETTER</a></li>
		<li><a href="//salumificiociriaci.it/punti-vendita">PUNTI VENDITA</a></li>
		<li><a href="//salumificiociriaci.it/contatti">CONTATTI</a></li>
		<li><a href="//salumificiociriaci.it/area-clienti">AREA CLIENTI</a></li>
	</ul>
</div>
<div class="col-12 copyright">
&copy; 2019	Salumificio Ciriaci. All Rights Reserved.
</div>
  </div>
		<!-- /.container -->
		
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
