<?php
/**
 * Template Name: Template azienda Salumificio Ciriaci
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<?php get_header(); ?>
<div class="container-fluid blocco-1-azienda">
<div class="row">
<img class="sfondo4" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/azienda-blocco-1.jpg" alt="Logo Salumificio Ciriaci" />
<div class="testo-slider-azienda">  
<h6>Il<br/>Salumificio</h6>
  <h4>Ciriaci</h4>
</div>
 <img class="sfondo5" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/blocco-1-azienda.png" alt="Sfondo blocco 1" />   

</div>
</div>

<div class="container-fluid blocco-2-azienda">
<div class="row riga-1-azienda-blocco-2">    
<div class="col-12">
        <h4>IL SALUMIFICIO CIRIACI</h4>
        <h5>Lavorazione e bontà dei tempi passati</h5>
        <p><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-verde-azienda-blocco-1.png" /></p>
</div>
</div>
<div class="row riga-2-azienda-blocco-2">
<div class="col-1"></div>
<div class="col-5">
<p>Il Salumificio Ciriaci nasce nel <strong>1978</strong> nel cuore delle Marche dalla passione e 
l’esperienza di Benito Ciriaci e sua moglie Elvira con l’idea di creare 
un’azienda capace di produrre prodotti di qualità, sani, genuini e lavorati 
con metodi tradizionali. 
Nel tempo il nome Ciriaci è diventato un vero e proprio simbolo della 
tradizione alimentare marchigiana, garante delle buone cose fatte in casa. 
Il marchio preserva tradizione e qualità, ricercando nelle origini le ricette 
per conservare intatti i profumi e i sapori di un tempo, portando a tavola i 
migliori prodotti marchigiani.
</p>
<p>Uno degli ingredienti segreti dell’azienda è il tempo, quello necessario alla 
stagionatura di lonze, lonzini, pancette, prosciutti, salami e salsicce. Gli 
insaccati vengono inoltre spostati ciclicamente in diversi ambienti, regolati 
con umidità e temperatura variabile per emulare le stagioni dell’anno e 
aspettare la giusta asciugatura. 
</p>
</div>
<div class="col-6">
<img class="immagine-1-azienda-blocco-2" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-1-azienda-blocco-2.png" alt="immagine azienda salumificio ciriaci" />
<img class="immagine-2-azienda-blocco-2" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-2-azienda-blocco-2.png" alt="immagine azienda salumificio ciriaci" />
</div>
</div>
</div>

<div class="container-fluid blocco-3-azienda">
<div class="row riga-1-azienda-blocco-3">
  <div class="col-4 foglia-sx-azienda-blocco-3"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/foglia-sx-azienda-blocco-3.png" alt="foglia sinistra Salumificio Ciriaci" /></div>    
<div class="col-4 titolo-missione">
        <h4>LA NOSTRA MISSIONE</h4>
        <h5>Storia di gesti, di esperienza, di passione</h5>
</div>
<div class="col-4 foglia-dx-azienda-blocco-3"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/foglia-dx-azienda-blocco-3.png" alt="foglia destra Salumificio Ciriaci" /></div> 
</div>
<div class="row riga-2-azienda-blocco-3">
<div class="col-6">
<img class="immagine-1-azienda-blocco-3" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-1-azienda-blocco-3.png" alt="immagine azienda salumificio ciriaci" />
<img class="immagine-2-azienda-blocco-3" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-2-azienda-blocco-3.png" alt="immagine azienda salumificio ciriaci" />
</div>

<div class="col-5">
  <p>La nostra mission è creare un’azienda capace di produrre prodotti di qualità, sani, genuini e lavorati con metodi che preservano la <strong>tradizione</strong>. Vogliamo ricercare nelle origini le ricette per <strong>conservare intatti i profumi e i sapori di un tempo</strong>, portando a tavola i migliori prodotti marchigiani.</p>
  <p>Gli animali sono controllati dalla nascita fino alla maturità, seguendo una dieta particolare per l’ottenimento di una carne di alta qualità, e crescendo in condizioni idonee nel rispetto della normativa vigente.</p>
  <p><strong>Storia di gesti, di esperienza, di passione e di lavoro artigianale</strong> che si racconta da anni e ogni giorno si ripete per portare in tavola il profumo e il sapore delle terre marchigiane.</p>
</div>
<div class="col-1"></div>
</div>
</div>
<div class="container-fluid blocco-4-azienda">
 <div class="row riga-1-azienda-blocco-4">
<div class="col-1"></div>
<div class="col-4">
  <h4>CERTIFICAZIONI<br/>E GARANZIA</h4>
  <h5>Qualità certificata</h5>
<p>La certificazione degli alimenti è una garanzia in più che Ciriaci ha voluto offrire ai suoi clienti con una linea di prodotti NON OGM marchio QM e una linea di carni bio e senza glutine: la lonza stagionata affumicata, il ciauscolo Igp e il prosciutto stagionato con osso IGP Norcia. 
</p>
<p>Il salumificio Ciriaci si avvale di un sistema di <strong>tracciabilità totale</strong> tramite l’assegnazioni di lotti di produzione che certifica il <strong>percorso produttivo</strong> e di <strong>filiera del prodotto</strong>.
</p>
</div>
<div class="col-1"></div>
<div class="col-6">
<img class="immagine-1-azienda-blocco-4" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-1-azienda-blocco-4.png" alt="immagine azienda salumificio ciriaci" />
<img class="immagine-2-azienda-blocco-4" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/azienda/immagine-2-azienda-blocco-4.png" alt="immagine azienda salumificio ciriaci" />
</div>
</div> 
<div class="container-fluid">
<div class="row riga-2-azienda-blocco-4">
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/igp.png" />
<h5>IGP - INDICAZIONE<br/>GEOGRAFICA PROTETTA</h5>
<p>Marchio di tutela giuridica della denominazione che viene attribuito dall’Unione europea agli alimenti le cui peculiari caratteristiche qualitative dipendono essenzialmente o esclusivamente dal territorio in cui sono stati prodotti.</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/igt.png" />
<h5>IGT - INDICAZIONE<br/>GEOGRAFICA TIPICA</h5>
<p>
Marchio di origine che viene attribuito
dall’Unione Europea a quei prodotti
agricoli aventi quali una determinata
qualità e la cui produzione,
trasformazione e/o elaborazione
avviene in un’area geografica
determinata.
</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/senza-glutine.png" />
<h5>SENZA<br/>GLUTINE</h5>
<p>
Questo marchio aiuta ad evidenziare al
consumatore celiaco l’idoneità del
prodotto rispetto alle sue esigenze
alimentari.
</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/bio-certificato-europeo.png" />
<h5>BIO - CERTIFICATO<br/>EUROPEO</h5>
<p>
Questo marchio garantisce la
conformità delle produzioni ottenute
con metodo biologico in tutte le fasi
della filiera di produzione, dal campo
alla tavola, in conformità alle norme
dell’Unione Europea.
</p>
</div>
</div>
</div>
</div>
<?php get_footer(); ?>