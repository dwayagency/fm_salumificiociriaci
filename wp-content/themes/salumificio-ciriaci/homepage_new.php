<?php
/**
 * Template Name: Homepage salumificio ciriaci
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<?php get_header(); ?>

<div class="container-fluid blocco-1-homepage">
<div class="row">
  <img class="sfondo" src="http://salumificiociriaci.it/wp-content/uploads/2019/07/ProductAmbient095_PAN_6133_A17_Path_2_pattern.jpg" alt="Logo Salumificio Ciriaci" />
<img class="logo" src="http://salumificiociriaci.it/wp-content/uploads/2019/07/Logo_A17_Rectangle_2_pattern.png" alt="Logo Salumificio Ciriaci" />
<img class="sfondo2" src="http://salumificiociriaci.it/wp-content/uploads/2019/07/verde_slider.png" alt="Logo Salumificio Ciriaci" />
</div>
</div>
<div class="container-fluid blocco-2-homepage">
<div class="row">
<div class="col-4"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/logo-azienda.png" class="logo-home-blocco-2" alt="logo salumificio ciriaci"/></div>
<div class="col-4 titolo-blocco-2"><h3>IL SALUMIFICIO</h3><h2>CIRIACI</h2>
<p class="icona-verde-homepage"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/icona_verde_1.png" alt="icona verde" /></p>
<p class="sottotitolo_azienda-home">LAVORAZIONE E BONTà DEI TEMPI PASSATI</p>
<p class="testo-azienda-home">Il <strong>Salumificio Ciriaci</strong> nasce nel <strong>1978</strong> nel cuore
delle Marche dalla passione e l’esperienza di
Benito Ciriaci e sua moglie Elvira con l’idea di
creare un’azienda capace di produrre
prodotti di qualità, sani, genuini e lavorati con
metodi tradizionali.</p>
<p class="testo-azienda-home">Nel tempo il nome Ciriaci è diventato un vero
e proprio simbolo della tradizione alimentare
marchigiana, garante delle <strong>buone cose fatte
in casa</strong>. Il marchio preserva tradizione e
qualità, ricercando nelle origini le ricette per
conservare intatti i <strong>profumi</strong> e i <strong>sapori</strong> di un
tempo, portando a tavola i migliori prodotti
marchigiani.</p>
<p class="bottone-verde"><a href="#" >scopri di più</a></p>
</div>
<div class="col-4 immagini-aziende"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/azienda-1.png" alt="Azienda Salumificio Ciriaci" class="azienda-1-homepage" />
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/azienda-2.png" alt="Azienda Salumificio Ciriaci" class="azienda-2-homepage" />
</div>
</div>
</div>
<div class="container-fluid blocco-3-homepage">
<div class="row">
<div class="col-8"><img class="immagine-territorio-homepage" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/nostro_territorio_immagine.png" /></div>
<div class="col-4">

<h3>IL NOSTRO</h3>
<h2>TERRITORIO</h2>

<h5>
<div class="row">
<div class="col-2"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/icone-verdi-territorio-sinistra.png" class="foglie-sinistra" /></div><div class="col-8">NEL CUORE DELLA VAL D’ASO,<br/>
NEL CUORE DEI CONSUMATORI</div><div class="col-2"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/icone-verdi-territorio-destra.png" class="foglie-destra" /></div>
</h5>
<p>
Immersi in un paesaggio unico nel cuore della
Val d’Aso, in provincia di Fermo, si staglia in
una struttura moderna lo stabilimento
produttivo Ciriaci. È in questa terra operosa
che la famiglia Ciriaci negli anni ‘70 avviò
l’attività di produzione artigianale di affettati
e insaccati. Nel tempo questo nome è
diventato vero e proprio simbolo della qualità
che la contraddistingue. La sede di Ortezzano
è all’avanguardia e di grandi dimensioni con un
ampio punto vendita, dove si intuisce la cura e
la passione di un’azienda capace di
interpretare i cambiamenti senza perdere i
valori e le tradizioni.
</p>
<p class="bottone-bianco">
<a href="#">Scopri di più</a>
</p>
</div>
</div>
</div>
<div class="container-fluid blocco-4-homepage">
<div class="container">
<div class="row">
<div class="col-12">
<h3>LE NOSTRE</h3>
<h2>CERTIFICAZIONI</h2>
</div>
</div>
<div class="row">
<div class="col-12">
<P class="qualita-certificata">qualità certificata</P>
</div>
</div>
<div class="row">
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/igp.png" />
<h5>IGP - INDICAZIONE GEOGRAFICA PROTETTA</h5>
<p>Marchio di tutela giuridica della denominazione che viene attribuito dall’Unione europea agli alimenti le cui peculiari caratteristiche qualitative dipendono essenzialmente o esclusivamente dal territorio in cui sono stati prodotti.</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/igt.png" />
<h5>IGT - INDICAZIONE GEOGRAFICA TIPICA</h5>
<p>
Marchio di origine che viene attribuito
dall’Unione Europea a quei prodotti
agricoli aventi quali una determinata
qualità e la cui produzione,
trasformazione e/o elaborazione
avviene in un’area geografica
determinata.
</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/senza-glutine.png" />
<h5 class="sottotitolo-senzaglutine">SENZA GLUTINE</h5>
<p>
Questo marchio aiuta ad evidenziare al
consumatore celiaco l’idoneità del
prodotto rispetto alle sue esigenze
alimentari.
</p>
</div>
<div class="col-3 grandezza-img">
<img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/bio-certificato-europeo.png" />
<h5>BIO - CERTIFICATO EUROPEO</h5>
<p>
Questo marchio garantisce la
conformità delle produzioni ottenute
con metodo biologico in tutte le fasi
della filiera di produzione, dal campo
alla tavola, in conformità alle norme
dell’Unione Europea.
</p>
</div>
</div>
</div>
</div>
<div class="container-fluid blocco-5-homepage">

  <div class="col-12">
<h3>I NOSTRI</h3>
<h2>MIGLIORI PRODOTTI</h2> 
<h5>LE NOSTRE ECCELLENZE</h5>

</div>

  <div class="col-12">
    <div class="container">
  <?php echo do_shortcode( '[soc_slider_shortcode id="24"]' ); ?></div>

</div>
</div>
<div class="container-fluid blocco-6-homepage">
<div class="row">
<div class="col-8"><img class="immagine-territorio-homepage" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/immagine-amanti-del-maiale.png" /></div>
<div class="col-4">

<div class="row">
<div class="col-1"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/icone-verdi-territorio-sinistra.png" class="foglie-sinistra" /></div><div class="col-10">
<h3>PER AMANTI</h3>
<h2>DEL MAIALE</h2>

</div><div class="col-1"><img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/homepage/icone-verdi-territorio-destra.png" class="foglie-destra" /></div>
<p>
Mangiare è una necessità, Lorem ipsum dolor
sit amet, consectetuer adipiscing elit.
Lorem ipsum dolor sit amet, consectetuer
adipiscing elit, sed diam nonummy nibh euismod
tincidunt ut laoreet dolore magna aliquam erat
volutpat. Ut wisi enim ad minim.

</p>
<p>
MANGIARE INTELLIGENTEMENTE È UN’ARTE.
</p>
</div>
</div> 
</div>
</div>
<div class="container-fluid blocco-7-homepage">
<h3>LE NOSTRE</h3>
<h2>RICETTE PER TE</h2>
<h5>PENSATE CON LA BONTÀ DEI NOSTRI PRODOTTI</h5>
<div class="container">
<?php echo do_shortcode( '[smoothcategory catg_slug="ricette"]' ); ?>

</div>  

</div>
<div class="container-fluid newsletter">
<h3>RESTIAMO</h3>
<h2>IN CONTATTO</h2>
<?php echo do_shortcode( '[contact-form-7 id="5" title="Contact form 1"]' ); ?>

</div>
<?php get_footer(); ?>
