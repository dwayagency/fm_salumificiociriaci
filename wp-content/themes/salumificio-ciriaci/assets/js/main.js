jQuery(document).ready(function() {
	jQuery('#owl-example').owlCarousel({
        loop: true,
        stagePadding: 50,
        items: 3,
        margin: 0,
        singleItem: false,
        dots: false,
        nav: true,
        responsiveClass: true,
        responsive:{
            0: {
                items: 1,
                nav: true
            },
            768: {
                items: 3,
                nav: true
            }
        }
    });
}