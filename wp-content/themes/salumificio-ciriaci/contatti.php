<?php
/**
 * Template Name: Template contatti Salumificio Ciriaci
 * Template Post Type: post, page
 * The template for displaying RevSlider on a blank page
 */
?>
<?php get_header(); ?>
<div class="container-fluid blocco-1-contatti">
<div class="row">
<img class="sfondo6" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/contatti/contatti-blocco-1.png" alt="Immagine sfondo contatti Salumificio Ciriaci blocco 1" />
<div class="testo-slider-contatti">  
  <h4>Contatti</h4>
</div>
 <img class="sfondo7" src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/contatti/blocco-1-contatti.png" alt="Sfondo blocco 1" />   

</div>
</div>
<div class="container-fluid blocco-2-contatti">
    <div class="row">
        <div class="col-6 contattaci-colonna-1-blocco-2">
            <h4>CONTATTACI</h4>
            <div class="container">
            <div class="row">
            <div class="col-6">
                <h5 class="dove-siamo">Dove siamo</h5>
                <p class="dati-dove-siamo">
                    C.da San Massimo - 63851<br/>
Ortezzano (FM)<br/>
Marche - Italia<br/>
<strong>+39 0734 777134</strong>
                </p>
                <p class="bottone-mappa-google"><button type="button" class="btn btn-secondary">VISUALIZZA MAPPA GOOGLE</button></p>
</div>
<div class="col-6">
<h5 class="punti-vendita">Punti vendita</h5>
                <p class="dati-punti-vendita">
                Porto San Giorgio<br/>
Via Salvadori, n. 14<br/>
Sant´Elpidio a mare<br/>
Via Porta Romana
                </p>
                <p class="bottone-mappa-google"><button type="button" class="btn btn-secondary">VISUALIZZA MAPPA GOOGLE</button></p>
</div>
</div>
<div class="row">
    <div class="col-12">
        <p class="indirizzomailcontattaci">info@salumificiociriaci.it</p>
    </div>
</div>
</div>
        </div>
        <div class="col-6 contattaci-colonna-2-blocco-2">
            <div class="row">
                <div class="col-6"></div>
                <div class="col-6">
        <h4>SCRIVICI</h4> 
        <h5>Compila questo modulo<br/>
e ti risponderemo al più presto</h5>
<p><?php echo do_shortcode( '[contact-form-7 id="36" title="Contatti"]' ); ?></p>
        </div>
        </div>
</div>
</div>
</div>
<div class="container-fluid blocco-3-contatti">
   
<h3 class="restiamo">RESTIAMO</h3>
 <h2 class="in-contatto">IN CONTATTO</h2>
<p class="inserisci-mail">Inserisci il tuo indirizzo email</p>
<p><?php echo do_shortcode( '[contact-form-7 id="37" title="Restare in contatto"]' ); ?></p>
</div>
<div class="container-fluid blocco-4-contatti">
    <img src="//salumificiociriaci.it/wp-content/themes/salumificio-ciriaci/images/contatti/mappa-contatti.png" alt="mappa google del salumificio ciriaci" />
</div>
<?php get_footer(); ?>